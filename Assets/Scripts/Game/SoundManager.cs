﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip grCatch, airCatch, userThrow, p1Wins, p2Wins, bounce;
    public static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        grCatch = Resources.Load<AudioClip>("Catch");
        airCatch = Resources.Load<AudioClip>("AerialCatch");
        userThrow = Resources.Load<AudioClip>("Throw");
        p1Wins = Resources.Load<AudioClip>("Player1Wins");
        p2Wins = Resources.Load<AudioClip>("Player2Wins");
        bounce = Resources.Load<AudioClip>("Bounce");


        audioSource = this.GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "Catch":
                audioSource.PlayOneShot(grCatch);
                break;

            case "AerialCatch":
                audioSource.PlayOneShot(airCatch);
                break;

            case "Throw":
                audioSource.PlayOneShot(userThrow);
                break;

            case "p1Wins":
                audioSource.PlayOneShot(p1Wins);
                break;

            case "p2Wins":
                audioSource.PlayOneShot(p2Wins);
                break;

            case "bounce":
                audioSource.PlayOneShot(bounce);
                break;
        }
    }
}
