﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disk : MonoBehaviour
{
    private Rigidbody2D rb;
    private TrailRenderer trail;
    public float startTimer;
    public Vector2 startVal;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        trail = GetComponent<TrailRenderer>();
        startTimer = 3;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(startTimer > 0)
        {
            startTimer -= Time.deltaTime;
        }

        else
        {
            rb.AddForce(startVal * (Random.Range(0, 2) * 2 - 1));
            this.enabled = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("FireWall"))
        {
            SoundManager.PlaySound("bounce");
        }
    }

    public void SetColor(Color newColor)
    {
        trail.startColor = newColor;
    }

}
