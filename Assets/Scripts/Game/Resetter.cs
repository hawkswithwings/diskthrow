﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resetter : MonoBehaviour
{
    public GameObject p1, p2, disk, winMessage;
    public Text p1Text, p2Text, winText;

    private GameObject[] platforms;

    private string p1Phrase, p2Phrase;
    private int p1Count, p2Count;
    private Vector3 p1Start, p2Start, diskStart;

    public float timer;

    public string phase;

    private bool p1Won;

    // Start is called before the first frame update
    void Awake()
    {
        platforms = GameObject.FindGameObjectsWithTag("Platform");

        p1Won = false;
        phase = "Waiting";

        p1Start = p1.transform.position;
        p2Start = p2.transform.position;
        diskStart = disk.transform.position;

        p1Phrase = "P1 Wins: ";
        p2Phrase = "P2 Wins: ";

        p1Count = 0;
        p2Count = 0;
        timer = 0;

        p1Text.text = p1Phrase + p1Count;
        p2Text.text = p2Phrase + p2Count;

        this.gameObject.SetActive(false);
    }

    private void Update()
    {

        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                if (phase.Equals("Waiting"))
                {

                    winMessage.SetActive(false);

                    p1.transform.position = p1Start;
                    p2.transform.position = p2Start;

                    p1.SetActive(true);
                    p2.SetActive(true);

                    phase = "Starting";

                    disk.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    disk.SetActive(true);
                    disk.transform.position = diskStart;

                    foreach(GameObject curPlat in platforms)
                    {
                        curPlat.GetComponent<Platform>().Restart();
                    }

                    timer = 3;
                }
                else if (phase.Equals("Starting"))
                {
                    if (p1Won)
                    {
                        disk.GetComponent<Rigidbody2D>().AddForce(disk.GetComponent<Disk>().startVal * -1.0f);
                    }
                    else
                    {
                        disk.GetComponent<Rigidbody2D>().AddForce(disk.GetComponent<Disk>().startVal);
                        
                    }
                    phase = "Waiting";
                    this.gameObject.SetActive(false);
                }
            }
        }
    }

    public void Restart(bool isPlayerTwo)
    {
        if (isPlayerTwo)
        {
            p1Won = true;
            p1Count++;
            winText.text = "Player One Wins!";
            SoundManager.PlaySound("p1Wins");
        }
        else
        {
            p1Won = false;
            p2Count++;
            winText.text = "Player Two Wins!";
            SoundManager.PlaySound("p2Wins");
        }

        p1Text.text = p1Phrase + p1Count;
        p2Text.text = p2Phrase + p2Count;

        p2.SetActive(false);
        p1.SetActive(false);
        
        winMessage.SetActive(true);
        

        timer = 3;
    }
}
