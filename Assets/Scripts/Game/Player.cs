﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D rb;
    public Vector2 movement, jumpMove;

    private bool breakNextBroken, airCaught;

    private string hor, ver, action, jump;
    private float h, v;

    private GameObject disk;
    private float timer, jumpCoolDown;
    private string state;

    public bool isPlayerTwo;
    public float throwForce, jumpForce;

    public float throwTime, catchTime, recovTime, jumpTime, noJumpTime;

    public Resetter resetter;

    public Color heldColor, defColor, trailCol;

    public GameObject cantJump;

    private SpriteRenderer sprite;
    public LayerMask platLayer;
    private const string idleState = "Idle", 
        hasDiskState = "HasDisk",
        caughtState = "Caught",
        recovState = "Recovering",
        jumpState = "Jumping";

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        movement = new Vector2();

        state = idleState;
        timer = 0;

        hor = "Horizontal";
        ver = "Vertical";
        action = "Throw";
        jump = "Jump";

        breakNextBroken = false;
        airCaught = false;

        if (isPlayerTwo)
        {
            hor += "2";
            ver += "2";
            action += "2";
            jump += "2";
        }
    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxisRaw(hor);
        v = Input.GetAxisRaw(ver);


        if (state.Equals(hasDiskState) && Input.GetButtonDown(action))
        { 
            Throw();
        }
        else if (state.Equals(idleState) && Input.GetButtonDown(jump) && jumpCoolDown <= 0)
        {
            //Ther user can jump

            Jump();
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                if (state.Equals(hasDiskState))
                    Throw();
                else if (state.Equals(caughtState))
                {
                    sprite.color = heldColor;
                    state = hasDiskState;
                    timer = throwTime;
                }
                else if (state.Equals(recovState))
                    state = idleState;
                else if (state.Equals(jumpState))
                    Land();
            }
        }
        
        if(jumpCoolDown > 0)
        {
            jumpCoolDown -= Time.deltaTime;
            if(jumpCoolDown <= 0)
                cantJump.SetActive(false);
        }

        movement = new Vector2(h, v) * moveSpeed;
    }

    private void Jump()
    {
        this.transform.localScale = new Vector3(1, 1, 1);
        state = jumpState;
        
        jumpMove = new Vector2(h, v*1.25f) * jumpForce;

        timer = jumpTime;
        this.gameObject.layer = LayerMask.NameToLayer("Jumping");
    }
    private void Land()
    {
        this.transform.localScale = new Vector3(0.75f, 0.75f, 1.0f);
        
        timer = 0;
        this.gameObject.layer = LayerMask.NameToLayer("Default");

        if (airCaught)
        {
            sprite.color = heldColor;
            state = hasDiskState;
            timer = throwTime;
        }
        else
        {
            state = idleState;
        }
        jumpCoolDown = noJumpTime;
        cantJump.SetActive(true);

        Collider2D plat = Physics2D.OverlapPoint(this.transform.position, platLayer);

        if (plat != null)
        {
            if (plat.gameObject.GetComponent<Platform>().GetState().Equals("#Broken"))
            {
                LoseAndReset();
            }
        }
    }

    private void FixedUpdate()
    {
        if (state.Equals(idleState))
        {
            rb.velocity = movement;
        }
        else if (state.Equals(hasDiskState))
        {
            rb.velocity = movement * 0.5f;
        }
        else if (state.Equals(jumpState))
        {
            rb.velocity = jumpMove;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*Collider2D plat = Physics2D.OverlapPoint(this.transform.position, platLayer);
        string curPlatState = plat.gameObject.GetComponent<Platform>().GetState();*/


        if (collision.gameObject.CompareTag("Disk"))
        {
            disk = collision.gameObject;
            if (state.Equals(jumpState))
            {
                AirCatch();
            }
            else
            {
                Catch(collision.relativeVelocity);
            }
        }
        if (state.Equals(caughtState))
        {

            if ((collision.gameObject.CompareTag("FireWall") || collision.gameObject.CompareTag("Net")) && breakNextBroken)
            {
                LoseAndReset();
            }

            if (collision.gameObject.CompareTag("Platform") && !airCaught)
            {
                // Ya got knocked into the pit, loser
                LoseAndReset();
            }

        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (state.Equals(caughtState))
        {
            if ((collision.gameObject.CompareTag("FireWall") || collision.gameObject.CompareTag("Net")) && breakNextBroken)
                LoseAndReset();

            if (collision.gameObject.CompareTag("Platform"))
                // Ya got knocked into the pit, loser
                LoseAndReset();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            Platform curPlat = collision.gameObject.GetComponent<Platform>();
            if (curPlat.GetState().Equals("#Damaged"))
            {
                if (breakNextBroken)
                {
                    breakNextBroken = false;
                }
            }
        }
    }

    private void Catch(Vector2 speed)
    {
        SoundManager.PlaySound("Catch");
        //StartCoroutine(HitStop());
        Vector2 diskForce = speed * 2;
        disk.SetActive(false);

        rb.velocity = Vector2.zero;
        rb.AddForce(diskForce);

        
        state = caughtState;

        timer = catchTime;

        //Collider2D[] cols = Physics2D.OverlapPointAll(Vector3.Scale(new Vector3(1, 1, 0), this.transform.position), LayerMask.GetMask("Platform"));


        Collider2D plat = Physics2D.OverlapPoint(this.transform.position, platLayer);
        

        if (plat != null && !state.Equals(jumpState))
        {
            //breakNextBroken = !plat.gameObject.GetComponent<Platform>().DamagePlatform();

            string curPlatState = plat.gameObject.GetComponent<Platform>().GetState();

            if (curPlatState.Equals("#Pristine"))
            {
                plat.gameObject.GetComponent<Platform>().DamagePlatform();
            }
            else if (curPlatState.Equals("#Damaged") && !breakNextBroken)
            {
                breakNextBroken = true;
                plat.gameObject.GetComponent<Platform>().setBroken();
            }
            else if (breakNextBroken)
            {
                LoseAndReset();
            }

            /*else if(curPlatState.Equals("#Broken"))
            {
                LoseAndReset();
            }*/

        }

    }

    private void AirCatch()
    {
        SoundManager.PlaySound("AerialCatch");
        StartCoroutine(HitStop());
        airCaught = true;
        disk.SetActive(false);
    }

    private void Throw()
    {
        rb.velocity = Vector2.zero;
        Vector3 newPos = this.transform.position;

        Vector2 power = new Vector2(1, v) * throwForce;

        // Increase speed based on appropriate input
        if (isPlayerTwo)
        {
            if (v > 0)
            {
                power.x -= 10;
            }
            else if (v < 0)
            {
                power.x += 10;
            }
        }
        else
        {
            if (v > 0)
            {
                power.x += 10;
            }
            else if (v < 0)
            {
                power.x -= 10;
            }
        }


        // Increase the speed thrown if they throw immediately
        if(timer > 1.8)
        {
            power.x *= 2.0f;
        }

        // Halve it if they wait to 1 second
        else if(timer < 1)
        {
            power.x *= 0.5f;
        }
        // quarter it if they wait to half a second
        else if (timer < 0.5)
        {
            power.x *= 0.25f;
        }

        if (isPlayerTwo)
        {
            power.x *= -1;
            newPos.x -= 0.55f;
        }
        else
        {
            newPos.x += 0.55f;
        }

        disk.transform.position = newPos;
       
        disk.SetActive(true);
        disk.GetComponent<Disk>().SetColor(trailCol);
        disk.GetComponent<Rigidbody2D>().AddForce(power);
        

        timer = 0.5f;
        state = recovState;
        sprite.color = defColor;

        airCaught = false;
        SoundManager.PlaySound("Throw");
    }

    private void LoseAndReset()
    {
        state = idleState;
        timer = 0;
        resetter.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
        jumpCoolDown = 0;
        cantJump.SetActive(false);
        resetter.Restart(isPlayerTwo);
    }
    private IEnumerator HitStop()
    {
        Time.timeScale = 0.0f;

        yield return new WaitForSecondsRealtime(0.5f);

        Time.timeScale = 1.0f;
        // say wait for real seconds
    }

}
