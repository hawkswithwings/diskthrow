﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private SpriteRenderer curSprite;
    private PolygonCollider2D col;
    public Sprite undamaged;
    public Sprite damaged;
    private string state; // "#Pristine", "#Damaged", "#Broken"
    private bool becomeBroken;

    private const string pristineState = "#Pristine",
        damagedState = "#Damaged",
        brokenState = "#Broken";

    // Start is called before the first frame update
    void Start()
    {
        becomeBroken = false;
        curSprite = this.GetComponent<SpriteRenderer>();
        col = this.GetComponent<PolygonCollider2D>();
        state = pristineState;
    }


    public bool DamagePlatform()
    {
        if (!curSprite.sprite.Equals(damaged))
        {
            curSprite.sprite = damaged;
            state = damagedState;
            return true;
        }
        else
            return false;
        /*else
        {
            curSprite.sprite = null;
            this.gameObject.layer = 11;
        }*/
    }

    public void DestroyIt()
    {
        state = brokenState;
        curSprite.sprite = null;
        //this.gameObject.layer = 11;
        col.isTrigger = false;
    }

    public string GetState()
    {
        return state;
    }
    
    public void Restart()
    {
        state = pristineState;
        curSprite.sprite = undamaged;
        col.isTrigger = true;
    }

    public void setBroken()
    {
        becomeBroken = true;
    }

    public bool getWillBreak()
    {
        return becomeBroken;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.gameObject.CompareTag("Player1") || collision.gameObject.CompareTag("Player2")) && becomeBroken)
        {
            becomeBroken = false;
            this.DestroyIt();

        }
        
    }
}
